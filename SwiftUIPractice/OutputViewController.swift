//
//  OutputViewController.swift
//  SwiftUIPractice
//
//  Created by JONNY  on 11/13/20.
//

import UIKit



class OutputViewController: UIViewController {
    
    @IBOutlet weak var textViewOutPut: UITextView!
    @IBOutlet weak var getImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
extension OutputViewController: SendDelegate{
    func send(name: String, address: String, email: String, phone: String){
        textViewOutPut.text = "Name : \(name) \nAddress : \(address)\nEmail : \(email) \nPhone Number : \(phone)"
    }
    
    func sendImage(image: UIImage, color: UIColor){
        getImage.image = image
        getImage.tintColor = color
    }
}



