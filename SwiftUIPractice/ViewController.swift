//
//  ViewController.swift
//  SwiftUIPractice
//
//  Created by JONNY on 11/13/20.
//

import UIKit


protocol SendDelegate{
    func send(name: String, address: String, email: String, phone: String)
    func sendImage(image: UIImage, color: UIColor)
}

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var address: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    @IBOutlet weak var phone: UITextField!
    @IBOutlet weak var senderImage: UIImageView!
    
    var delegate : SendDelegate?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        sendBtn.backgroundColor = UIColor.black
        sendBtn.layer.cornerRadius = sendBtn.frame.height/2
        sendBtn.setTitleColor (UIColor.white, for:.normal)
        sendBtn.layer.shadowColor = UIColor.red.cgColor
        sendBtn.layer.shadowRadius = 3
        
        userName.delegate = self
        address.delegate = self
        email.delegate = self
        phone.delegate = self
        email.keyboardType = .emailAddress
        phone.returnKeyType = .done
        

    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField {
            print(nextField.tag)
             nextField.becomeFirstResponder()
          } else {
             textField.resignFirstResponder()
          }
          return false
    }
    
    
    
    @IBAction func SendButton(_ sender: Any) {
        
        let personIcon = UIImage(systemName: "person.fill")
        let outputView = storyboard?.instantiateViewController(identifier: "output") as! OutputViewController
        outputView.modalPresentationStyle = .fullScreen
        self.delegate = outputView
        present(outputView, animated: true, completion: nil)
        
        delegate?.send(name: userName.text!, address: address.text!, email: email.text!, phone: phone.text!)
        delegate?.sendImage(image: personIcon!, color:senderImage.tintColor)
    }
}

